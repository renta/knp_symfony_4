#!/usr/bin/env bash

# VARS #
echo -e "-- Setting global variables\n"
MYSQL_DATABASE=symfony4_space_bar
MYSQL_USER=root
MYSQL_PASSWORD=123

echo -e "-- Adding Ondřej Surý ppa's \n"
add-apt-repository ppa:ondrej/apache2
add-apt-repository ppa:ondrej/php


echo -e "-- Updating packages list\n"
apt-get update
apt-get -y autoremove


echo -e "-- Installing git\n"
apt-get install git

# APACHE #
echo -e "-- Installing Apache web server\n"
apt-get install -y apache2
a2enmod rewrite
sed -i -e 's/www-data/vagrant/g' /etc/apache2/envvars


# MYSQL #
echo -e "-- Installing MySQL server\n"
debconf-set-selections <<< "mysql-server mysql-server/root_password password ${MYSQL_PASSWORD}"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${MYSQL_PASSWORD}"

apt-get install -y mysql-server  mysql-client


# PHP 5.6 #
echo -e "-- Installing php 7.2 and it's packages\n"
apt-get install -y php7.2 php7.2-cli php7.2-mysql php7.2-sqlite3 php7.2-mbstring php7.2-curl php7.2-xml  php7.2-zip php7.2-intl php-xdebug libapache2-mod-php7.2 php-apcu


# VIRTUAL HOST FOR APACHE #
echo -e "-- Updating vhost file\n"
cat > /etc/apache2/sites-available/learn-symfony4.loc.conf <<EOF
<VirtualHost *:80>
    ServerName learn-symfony4.loc
    ServerAlias www.learn-symfony4.loc
    DocumentRoot /vagrant/public
    
    <Directory /vagrant>
        AllowOverride All
        Require all granted
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeScript assets
    <Directory /vagrant>
        Options FollowSymlinks
    </Directory>

    ErrorLog /var/log/apache2/learn-symfony4_error.log
</VirtualHost>
EOF
a2dissite 000-default
a2ensite learn-symfony4.loc


echo -e "-- Restarting Apache\n"
service apache2 restart


echo -e "-- Installing Composer"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer


# echo -e "-- Installing Composer packages and optimize autoload"
# cd /vagrant
# composer install

# echo -e "-- Creating a DB with Doctrine cli, run migrations, install fixtures"
# php bin/console doctrine:database:create
# php bin/console doctrine:migrations:migrate -n -q
# php bin/console doctrine:fixtures:load -y