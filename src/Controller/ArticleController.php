<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Service\SlackClient;
use Doctrine\ORM\EntityManagerInterface;
use Nexy\Slack\Client;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractController
{
    /**
     * @var Client
     */
    private $slack;
    /**
     * Currently unused: just showing a controller with a constructor!
     * @var bool
     */
    //private $isDebug;

    public function __construct(bool $isDebug, Client $slack)
    {
        $this->isDebug = $isDebug;
        $this->slack = $slack;
    }

    /**
     * @Route("/", name="app_homepage")
     *
     * @param ArticleRepository $repository
     *
     * @return Response
     */
    public function homepage(ArticleRepository $repository): Response
    {
        //return new Response('First page is ready.');
        //$repository = $em->getRepository(Article::class);
        $articles = $repository->findAllPublishedOrderedByNewest();
        return $this->render('article/homepage.html.twig', [
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/news/{slug}", name="article_show")
     *
     * @param Article $article
     * @param SlackClient $slack
     *
     * @return Response
     *
     * @throws \Http\Client\Exception
     */
    public function show(Article $article, SlackClient $slack): Response
    {
        /*return new Response(sprintf(
            'Future page to show one space article: "%s"',
            $slug
        ));*/
        //dump($this->getParameter('isBabak'));

        if ($article->getSlug() === 'khaaaaaan') {
            $slack->sendMessage('Khan', 'Ah, Kirk, my old friend...');
        }

        //$articleContent = $markdownHelper->parse($articleContent);

        return $this->render('article/show.html.twig', [
            'article' => $article
        ]);
        /*$html = $twigEnvironment->render('article/show.html.twig', [
            'title' => ucwords(str_replace('-', ' ', $slug)),
            'slug' => $slug,
            'comments' => $comments,
        ]);
        return new Response($html);*/
    }

    /**
     * @Route("/news/{slug}/heart", name="article_toggle_heart", methods={"POST"})
     *
     * @param Article $article
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     */
    public function toggleArticleHeart(Article $article, LoggerInterface $logger, EntityManagerInterface $em): JsonResponse
    {
        // TODO - actually heart/unheart the article!
        $logger->info('Article is being hearted!');

        $article->incrementHeartCount();
        $em->flush();

        return new JsonResponse(['hearts' => $article->getHeartCount()]);
        //or
        //return $this->json(['hearts' => rand(5, 100)]);
    }
}